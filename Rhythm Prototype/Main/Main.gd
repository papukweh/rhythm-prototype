extends Node

var song: Song
var beatMap: Array = []
var game_mode = 1

# All time units in ms
var elapsedTime: float = 0.0
var playHeadPosition: float = 0.0
var nextNote
var started: bool = false

const FIRST_POS = Vector2(500, 250)
const FIRST_DIR = 0

const CENTER = Vector2(15, -700)
const LEFT = Vector2(-650, -700)
const RIGHT = Vector2(650, -700)

const SFX = ["HIT", "SHOOT"]

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	game_mode = GLOBAL.GAME_MODE
	var difficulty = GLOBAL.DIFICULTY
	song = get_song()
	
	if game_mode == 0:
		beatMap = beatmap_from_list(song.get_beatmap(GLOBAL.MODE_NAMES[game_mode], difficulty))
	else:
		beatMap = bullets_from_list(song.get_beatmap(GLOBAL.MODE_NAMES[game_mode], difficulty), CENTER)
	$NoteManager/Countdown.connect("countdown_ended", self, "start_level")
	$SongPlayer.connect("finished", self, "finish_level")
	$Camera.init(game_mode)
	print("GAME_MODE", game_mode)
	init()

func finish_level() -> void:
	print('END!!')
	$NoteManager.over = true
	var score = 0
	var high_score = false
	var metadata = GLOBAL.load_metadata_info(song.metadataPath)
	if game_mode == 0:
		var screenpath = song.metadataPath.replace('.json', '.png')
		$NoteManager.prepare_for_screenshot()
		$Camera.prepare_for_screenshot($Game/Notes/Line2D)
		$Timer.start()
		yield($Timer, "timeout")
		$Screenshot.take_screenshot(screenpath)
		yield($Screenshot, "screenshot_taken")
		score = $NoteManager.score 
	elif $Game/Player:
		score = 1000*$NoteManager.score*(1+$Game/Player.life) + (500*$Game/Player.bonus)
		if $Game/Player.life > 0:
			AUDIO.play_se("VICTORY", -8)
		else:
			AUDIO.play_se("DEFEAT", -6)
	if score > metadata.high_score[GLOBAL.GAME_MODE]:
		high_score = true
		metadata['high_score'][GLOBAL.GAME_MODE] = score
		GLOBAL.save_song_metadata(song.metadataPath, metadata)
	if high_score and game_mode == 0:
		AUDIO.play_se("VICTORY", -8)
	elif game_mode == 0:
		AUDIO.play_se("DEFEAT",  -6)
	print('FINAL SCORE: {score}'.format({'score': score }))
	$NoteManager/End.enter(score, high_score)

func init() -> void:
	if game_mode == 0:
		$Game/Player.queue_free()
		$Game/Wall.queue_free()
	else:
		$Game/Player.connect("player_death", self, "finish_level")
	$NoteManager.init(song, $Game/Notes, game_mode)
	$NoteManager/Countdown.init(3)

func beatmap_from_list(onsets: Array) -> Array:
	print(onsets)
	var beatmap = []
	var prevPos = FIRST_POS
	var prevDir = FIRST_DIR
	var prevOnset = 0.0
	for onset in onsets:
		var newNote = Note.new((onset*1000)-1000, onset*1000, prevPos, prevDir, prevOnset)
		prevPos = newNote.position
		prevDir = newNote.direction
		prevOnset = newNote.pressTime
		beatmap.push_back(newNote)
	return beatmap

func bullets_from_list(onsets: Array, source: Vector2) -> Array:
	print(onsets)
	var beatmap = []
	var prevPos = 3
	var prevDir = 1
	for onset in onsets:
		var newNote = Projectile.new((onset*1000), prevPos, prevDir)
		prevPos = newNote.position_index
		prevDir = newNote.direction
		beatmap.push_back(newNote)
	print("BEATMAP RESULTANTE")
	print(beatMap)
	return beatmap

func get_song() -> Song:
	return GLOBAL.SONG

func start_level() -> void:
	AUDIO.set_sfx_volume(20)
	AUDIO.set_bgm_volume(-5)
	$SongPlayer.volume_db = AUDIO.get_bgm_volume()
	$SongPlayer.play_song(song)
	started = true
	$PulseAnimation.playback_speed = (song.BPM / 60.0) / 3.333333
	$PulseAnimation.play("pulse")
	$ColorAnimation.playback_speed = 21.0 / song.duration
	$ColorAnimation.play("color")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta) -> void:
	GLOBAL.beat_scale = $Game/PulseReference.scale
	GLOBAL.color_scale = $Game/ColorReference.color
	if started:
		elapsedTime += (delta * 1000)
		update_playhead()
		process_inputs()
		if len(beatMap) > 0 and started:
			spawn_notes()

func update_playhead() -> void:
	playHeadPosition = $SongPlayer.update_playhead(elapsedTime)
	#elapsedTime = playHeadPosition

func process_inputs() -> void:
	$NoteManager.update(elapsedTime, $Game/Notes/Line2D)

func spawn_notes() -> void:
	nextNote = beatMap.front()
	if nextNote and nextNote.spawnTime <= elapsedTime:
		if game_mode == 1:
			AUDIO.play_se(SFX[game_mode])
		$NoteManager.spawn(nextNote)
		beatMap.pop_front()
