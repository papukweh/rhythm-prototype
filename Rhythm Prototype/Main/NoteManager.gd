extends Node

var game_mode : int = 0
# 0 is Star Connect, 1 is Star Shooter

var bpm : float = 0
var over: bool = false
var song_duration : float = 0

const POINTS = {
	"MISS": 0.0, 
	"BAD": 5.0, 
	"GOOD": 10.0,
	"GREAT": 50.0,
	"EXCELLENT": 100.0
}

const MULTIPLIER_THRESHOLDS = {
	0: 1,
	5: 2,
	10: 3,
	20: 4,
	35: 8,
	50: 16
}

func build_multiplier_map():
	var map = []
	var keys = MULTIPLIER_THRESHOLDS.keys()
	var previousKey = keys[0]
	var previousValue = MULTIPLIER_THRESHOLDS[previousKey]
	keys.pop_front()
	for key in keys:
		var value = MULTIPLIER_THRESHOLDS[key]
		for j in range(key - previousKey):
			map.append(previousValue)
		previousKey = key
		previousValue = value

	print("Multiplier map")
	print(map)
	return map

var NOTE_SCENE: Resource = load("res://Connect/Prompt.tscn")
var BULLET_SCENE: Resource = load("res://Shooter/Bullet.tscn")
var score: float = 0.0
var multiplier: float = 1.0
var notes_hit: int = 0
var multiplier_map: Array = []

var notes: Array
var notes_node: Node

# A S
var playerInputs = PlayerInput.get_all_actions()

func _ready():
	game_mode = GLOBAL.GAME_MODE
	if game_mode == 0:
		multiplier_map = build_multiplier_map()
	elif game_mode == 1:
		$Footer.hide()

func prepare_for_screenshot() -> void:
	$Header.hide()
	$Footer.hide()

# Called when the node enters the scene tree for the first time.
func init(song: Song, node: Node, mode_arg: int) -> void:
	$Footer/Score.set_text("0")
	$Footer/Grade.set_text("")
	$Header/Progress.value = 0
	$Header/Progress.min_value = 0
	$Header/Progress.max_value = song.duration * 1000
	$Header/SongId.text = song.title+" - "+song.artist
	$Footer/Multiplier.set_text("1X")
	bpm = song.BPM
	notes = []
	notes_node = node
	game_mode = mode_arg
	song_duration = song.duration * 1000

func get_multiplier(notes_hit: int) -> int:
	if notes_hit >= len(multiplier_map):
		notes_hit = len(multiplier_map) - 1
	return multiplier_map[notes_hit]

func spawn(object) -> void:
	if not over:
		if game_mode == 0:
			spawn_note(object)
		elif game_mode == 1:
			spawn_bullet(object)

func spawn_note(note: Note) -> void:
	if note.pressTime >= song_duration:
		return
	var input = note.input
	var position = note.position
	var inputText = input.buttonAction
	notes_node.add_child(NOTE_SCENE.instance())
	var node = notes_node.get_child(len(notes_node.get_children()) - 1)
	node.init(note.spawnTime, note.pressTime, inputText, position)
	notes.push_back(node)
	node.connect("NOTE_HIT", self, "_on_Note_Hit")


func spawn_bullet(bullet: Projectile) -> void:
	if get_parent().elapsedTime + 200 >= song_duration:
		return
	var position = bullet.position
	var speed = bullet.speed
	var direction = bullet.direction
	notes_node.add_child(BULLET_SCENE.instance())
	var node = notes_node.get_child(len(notes_node.get_children()) - 1)
	node.init(position, bpm, speed)

func update(elapsedTime: float, line: Line2D) -> void:
	if over:
		return
	$Header/Progress.value = elapsedTime
	if game_mode == 0 and notes:
		var next = notes.front()
		while next and next.currentScale - next.idealScale <= -next.initialScale*0.25:
			next.queue_free()
			notes.pop_front()
			next = notes.front()
		while not next:
			notes.pop_front()
			next = notes.front()
		var input : String = next.input
		var not_input = "rhythm_action_1"
		if input == "rhythm_action_1":
			not_input = "rhythm_action_2"
		if Input.is_action_just_pressed(input):
			line.add_point(next.position)
			next.die_and_calculate_score()
			next.queue_free()
			notes.pop_front()
		elif Input.is_action_just_pressed(not_input):
			next.die_and_calculate_score(true)
			next.queue_free()
			notes.pop_front()
	elif game_mode == 1:
		score = $Header/Progress.value / $Header/Progress.max_value

func update_multiplier():
	multiplier = get_multiplier(notes_hit)
	$Footer/Multiplier.set_text("X"+str(multiplier))

func _on_Note_Hit(input: String, grade: String) -> void:
	if grade == "MISS" or grade == "BAD":
		notes_hit = 0
	else:
		notes_hit += 1
	update_multiplier()
	score += multiplier * POINTS[grade]
	$Footer/Score.set_text("%d" % [score])
	$Footer/Grade.set_text(grade)
