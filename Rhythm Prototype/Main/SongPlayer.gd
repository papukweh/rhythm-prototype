extends AudioStreamPlayer

var currentSong: Song
var lastReportedplayback: float = 0.0

func play_song_preview(song: Song) -> void:
	set_stream(song.audio)
	play()
	$Timer.start()


func play_song(song: Song) -> void:
	self.currentSong = song
	set_stream(currentSong.audio)
	play()

func stop_song() -> void:
	$Timer.stop()
	$Timer.wait_time = 20
	stop()

func update_playhead(elapsedTime: float) -> float:
	var reportedPlayback = get_playback_position() * 1000
	if lastReportedplayback == reportedPlayback:
		return (abs(elapsedTime - lastReportedplayback))/2
	else:
		lastReportedplayback = reportedPlayback
		return reportedPlayback


func _on_Timer_timeout():
	play()
