"""
Module: LibFMP.C6.C6S1_OnsetDetection
Author: Meinard Müller, Angel Villar-Corrales
License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License

This file is part of the FMP Notebooks (https://www.audiolabs-erlangen.de/FMP)
"""

import numpy as np
from numba import jit
from scipy import signal
from scipy.interpolate import interp1d
import librosa

def compute_novelty_energy(x, Fs=1, N=2048, H=128, gamma=10, norm=1):
	"""Compute energy-based novelty function

	Notebook: C6/C6S1_NoveltyEnergy.ipynb

	Args:
		x: Signal
		Fs: Sampling rate
		N: Window size
		H: Hope size
		gamma: Parameter for logarithmic compression
		norm: Apply max norm (if norm==1)

	Returns:
		novelty_energy: Energy-based novelty function
		Fs_feature: Feature rate
	"""
	x_power = x**2
	w = signal.hann(N)
	Fs_feature = Fs/H
	energy_local = np.convolve(x**2, w**2 , 'same')
	energy_local = energy_local[::H]
	if gamma!=None:
		energy_local = np.log(1 + gamma * energy_local)
	energy_local_diff = np.diff(energy_local)
	energy_local_diff = np.concatenate((energy_local_diff, np.array([0])))
	novelty_energy = np.copy(energy_local_diff)
	novelty_energy[energy_local_diff < 0] = 0
	if norm==1:
		max_value = max(novelty_energy)
		if max_value > 0:
			novelty_energy = novelty_energy / max_value
	return novelty_energy, Fs_feature


@jit(nopython=True)
def compute_local_average(x, M):
	"""Compute local average of signal

	Notebook: C6/C6S1_NoveltySpectral.ipynb

	Args:
		x: Signal
		M: Determines size (2M+1) in samples of centric window  used for local average

	Returns:
		local_average: Local average signal
	"""
	L = len(x)
	local_average = np.zeros(L)
	for m in range(L):
		a = max(m - M, 0)
		b = min(m + M + 1, L)
		local_average[m] = (1 / (2 * M + 1)) * np.sum(x[a:b])
	return local_average


def compute_novelty_spectrum(x, Fs=1, N=1024, H=256, gamma=100, M=10, norm=1):
	"""Compute spectral-based novelty function

	Notebook: C6/C6S1_NoveltySpectral.ipynb

	Args:
		x: Signal
		Fs: Sampling rate
		N: Window size
		H: Hope size
		gamma: Parameter for logarithmic compression
		M: Size (frames) of local average
		norm: Apply max norm (if norm==1)

	Returns:
		novelty_spectrum: Energy-based novelty function
		Fs_feature: Feature rate
	"""
	X = librosa.stft(x, n_fft=N, hop_length=H, win_length=N, window='hanning')
	Fs_feature = Fs / H
	Y = np.log(1 + gamma * np.abs(X))
	Y_diff = np.diff(Y)
	Y_diff[Y_diff < 0] = 0
	novelty_spectrum = np.sum(Y_diff, axis=0)
	novelty_spectrum = np.concatenate((novelty_spectrum, np.array([0.0])))
	if M > 0:
		local_average = compute_local_average(novelty_spectrum, M)
		novelty_spectrum = novelty_spectrum - local_average
		novelty_spectrum[novelty_spectrum < 0] = 0.0
	if norm == 1:
		max_value = max(novelty_spectrum)
		if max_value > 0:
			novelty_spectrum = novelty_spectrum / max_value
	return novelty_spectrum, Fs_feature


def principal_argument(v):
	"""Principal argument function

	Notebook: /C6/C6S1_NoveltyPhase.ipynb, see also C8/C8S2_InstantFreqEstimation.ipynb

	Args:
		v: value (or vector of values)

	Returns:
		w: Principle value of v
	"""
	w = np.mod(v + 0.5, 1) - 0.5
	return w


def compute_novelty_phase(x, Fs=1, N=1024, H=64, M=40, norm=1):
	"""Compute phase-based novelty function

	Notebook: C6/C6/C6S1_NoveltyPhase.ipynb

	Args:
		x: Signal
		Fs: Sampling rate
		N: Window size
		H: Hop size
		M: Determines size (2M+1) in samples of centric window  used for local average
		norm: Apply max norm (if norm==1)

	Returns:
		novelty_spectrum: Energy-based novelty function
		Fs_feature: Feature rate
	"""
	X = librosa.stft(x, n_fft=N, hop_length=H, win_length=N, window='hanning')
	Fs_feature = Fs/H
	phase = np.angle(X)/(2*np.pi)
	phase_diff = principal_argument(np.diff(phase, axis=1))
	phase_diff2 = principal_argument(np.diff(phase_diff, axis=1))
	novelty_phase = np.sum(np.abs(phase_diff2), axis=0)
	novelty_phase = np.concatenate( (novelty_phase, np.array([0, 0])) )
	if M > 0:
		local_average = compute_local_average(novelty_phase, M)
		novelty_phase =  novelty_phase - local_average
		novelty_phase[novelty_phase<0]=0
	if norm==1:
		max_value = np.max(novelty_phase)
		if max_value > 0:
			novelty_phase = novelty_phase / max_value
	return novelty_phase, Fs_feature


def compute_novelty_complex(x, Fs=1, N=1024, H=64, gamma=10, M=40, norm=1):
	"""Compute complex-domain novelty function

	Notebook: C6/C6S1_NoveltyComplex.ipynb

	Args:
		x: Signal
		Fs: Sampling rate
		N: Window size
		H: Hop size
		M: Determines size (2M+1) in samples of centric window  used for local average
		norm: Apply max norm (if norm==1)

	Returns:
		novelty_spectrum: Energy-based novelty function
		Fs_feature: Feature rate
	"""
	X = librosa.stft(x, n_fft=N, hop_length=H, win_length=N, window='hanning')
	Fs_feature = Fs/H
	mag = np.abs(X)
	if gamma > 0:
		mag = np.log(1 + gamma * mag)
	phase = np.angle(X)/(2*np.pi)
	phase_diff = np.diff(phase, axis=1)
	phase_diff = np.concatenate((phase_diff, np.zeros((phase.shape[0], 1))), axis=1)
	X_hat = mag*np.exp(2*np.pi*1j*(phase+phase_diff))
	X_prime = np.abs(X_hat - X)
	X_plus = np.copy(X_prime)
	for n in range(1, X.shape[0]):
		idx = np.where( mag[n,:] < mag[n-1,:] )
		X_plus[n, idx] = 0
	novelty_complex = np.sum(X_plus, axis=0)
	if M > 0:
		local_average = compute_local_average(novelty_complex, M)
		novelty_complex =  novelty_complex - local_average
		novelty_complex[novelty_complex<0]=0
	if norm==1:
		max_value = np.max(novelty_complex)
		if max_value > 0:
			novelty_complex = novelty_complex / max_value
	return novelty_complex, Fs_feature

def join_onsets(a, b, tolerance):
	final = []
	i = 0
	j = 0
	if a == []:
		return b
	while i < len(a) - 1 or j < len(b) - 1:
		if abs(a[i] - b[j]) <= tolerance:
			avg = (a[i] + b[j])/2
			if len(final) == 0 or abs(final[-1] - avg) > tolerance: 
				final.append(avg)
			i = min(len(a)-1, i+1)
			j = min(len(b)-1, j+1)
		elif i != len(a) -1 and a[i] < b[j] or j == len(b) - 1:
			if len(final) == 0 or abs(final[-1] - a[i]) > tolerance: 
				final.append(a[i])
			i = min(len(a)-1, i+1)
		else:
			if len(final) == 0 or abs(final[-1] - b[j]) > tolerance: 
				final.append(b[j])
			j = min(len(b)-1, j+1)
	return final

def intersect_onsets(a, b, tolerance):
	final = []
	i = 0
	j = 0
	if a == []:
		return b
	while i < len(a) - 1 and j < len(b) - 1:
		if abs(a[i] - b[j]) <= tolerance:
			avg = (a[i] + b[j])/2
			if len(final) == 0 or abs(final[-1] - avg) > tolerance: 
				 final.append(avg)
			i = min(len(a), i+1)
			j = min(len(b), j+1)
		elif a[i] < b[j]:
			i = min(len(a)-1, i+1)
		else:
			j = min(len(b)-1, j+1)
	return final

def compute_energy(y, Fs):
	novelty_energy, Fs1 = compute_novelty_energy(y, Fs)
	onsets_energy = librosa.onset.onset_detect(y, Fs, novelty_energy, units='time')
	return onsets_energy

def compute_spectral(y, Fs):
	novelty_spectral, Fs4 = compute_novelty_spectrum(y, Fs)
	onsets_spectral = librosa.onset.onset_detect(y, Fs, novelty_spectral, units='time')
	return onsets_spectral

def compute_complex(y, Fs):
	novelty_complex, Fs2 = compute_novelty_complex(y, Fs)
	onsets_complex = librosa.onset.onset_detect(y, Fs, novelty_complex, units='time')
	return onsets_complex
