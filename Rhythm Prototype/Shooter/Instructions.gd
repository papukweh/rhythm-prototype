extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	$Panel/Player.tutorial = true
	$Panel/Player.direction = Vector2(0,0)
	$Animation.play("tutorial")

func enter():
	show()
	$Animation.play("tutorial")

func exit():
	hide()
	$Animation.stop()
