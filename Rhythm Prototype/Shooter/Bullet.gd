extends KinematicBody2D

export var color : Color = Color.white
const ACCELERATION : float = 3000.0
const MAX_SPEED : float = 6000.0
var velocity : Vector2 = Vector2(0,0)
const direction : Vector2 = Vector2(0,1)

const BASE_SCALE : Vector2 = Vector2(1, 1)

func _ready():
	$Star.color = color

func init(position_arg: Vector2, scale_arg: float, speed: float) -> void:
	self.global_position = position_arg
	#self.scale *= scale_arg
	velocity = direction * speed

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	self.rotate(PI/(delta*800))
	#$Star.scale = BASE_SCALE * GLOBAL.beat_scale
	$StarOutline.scale = BASE_SCALE * GLOBAL.beat_scale
	$Star.color = GLOBAL.color_scale
	#$Light2D.color = GLOBAL.color_scale
	velocity = velocity + direction*ACCELERATION*delta
	velocity = velocity.clamped(MAX_SPEED)
	velocity = move_and_slide(velocity)

func _on_TTL_timeout():
	queue_free()
