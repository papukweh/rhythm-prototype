extends Control

func enter(score: float, is_high_score: bool) -> void:
	get_tree().paused = true
	$Panel/VBoxContainer/ScoreValue.set_text(str(int(score)))
	if is_high_score:
		$Panel/VBoxContainer/HiScore.show()
	show()

func _on_Restart_pressed() -> void:
	get_tree().paused = false
	get_tree().change_scene("res://Main/Main.tscn")


func _on_Quit_pressed() -> void:
	get_tree().paused = false
	get_tree().change_scene("res://Menu/Main_Menu.tscn")

