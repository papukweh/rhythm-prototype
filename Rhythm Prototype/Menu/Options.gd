extends Control

const options_text = [
	"Sets screen resolution",
	"Display mode",
	"Base volume",
	"Sound effects volume",
	"Music volume",
]

func enter():
	show()

func exit():
	hide()

# Called when the node enters the scene tree for the first time.
func _ready():
	var i = 0
	for option in $Overview/Values.get_children():
		option.connect("pressed", self, "set_text", [i])
		i += 1
	
	for opt in DISPLAY.get_available_resolutions():
		$Overview/Values/Resolution.add_item(opt)
	
	$Overview/Values/Resolution.selected = DISPLAY.get_current_res()
	
	for opt in DISPLAY.get_modes():
		$Overview/Values/Display.add_item(opt)
	
	$Overview/Values/Display.selected = DISPLAY.get_current()
	
	$Overview/Values/Volume.set_value(AUDIO.get_master_volume())
	$Overview/Values/BGM.set_value(AUDIO.get_bgm_volume())
	$Overview/Values/SFX.set_value(AUDIO.get_sfx_volume())

func set_text(index):
	$Top_Panel/Description.set_text(options_text[index])


func _on_Resolution_item_selected(ID):
	DISPLAY.set_resolution(ID)


func _on_Display_item_selected(ID):
	DISPLAY.set_mode(ID)


func _on_Volume_value_changed(value):
	AUDIO.set_master_volume(value)


func _on_SFX_value_changed(value):
	AUDIO.set_sfx_volume(value)


func _on_BGM_value_changed(value):
	AUDIO.set_bgm_volume(value)
