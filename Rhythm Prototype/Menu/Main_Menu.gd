extends Control

var current : Node = null

func _ready():
	AUDIO.play_bgm("MENU_THEME", true)
	$RedStar.color = Color.red
	$BlueStar.color = Color.blue
	$BlueStar.position *= (OS.get_real_window_size() / Vector2(1080, 720))
	$RedStar.position *= (OS.get_real_window_size() / Vector2(1080, 720))
	$RedStar.set_process(false)
	$BlueStar.set_process(false)

func _input(input: InputEvent):
	if input.is_action_pressed("ui_cancel"):
		if current:
			current.exit()
			current = null


func _on_Quit_pressed():
	get_tree().quit()


func _on_Settings_pressed():
	current = $Options
	current.enter()


func _on_Play_pressed():
	current = $Load
	current.enter()
