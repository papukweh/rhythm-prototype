extends Control

var current : Node = null

func _ready():
	AUDIO.play_bgm("MENU_THEME", true)

func _input(input: InputEvent):
	if input.is_action_pressed("ui_cancel"):
		if current:
			current.exit()
			enable_buttons()
			current = null

func enable_buttons() -> void:
	$Main/Play.disabled = false
	$Main/Tutorial.disabled = false
	$Main/Quit.disabled = false

func disable_buttons() -> void:
	$Main/Play.disabled = true
	$Main/Tutorial.disabled = true
	$Main/Quit.disabled = true


func _on_Quit_pressed():
	AUDIO.play_se("ENTER_MENU", 12)
	get_tree().quit()


func _on_Settings_pressed():
	current = $Options
	AUDIO.play_se("ENTER_MENU", 12)
	disable_buttons()
	current.enter()


func _on_Play_pressed():
	current = $Load
	disable_buttons()
	current.enter(1)


func _on_Tutorial_pressed():
	current = $Instructions
	AUDIO.play_se("ENTER_MENU", 12)
	disable_buttons()
	current.enter()
