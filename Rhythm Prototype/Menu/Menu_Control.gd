extends Control

var current = 0

func _ready():
	$StarConnect.margin_bottom *= (OS.get_real_window_size().y / 720)
	$StarConnect.margin_right *= (OS.get_real_window_size().x / 1080)
	$StarShooter.margin_bottom *= (OS.get_real_window_size().y / 720)
	$StarShooter.margin_right *= (OS.get_real_window_size().x / 1080)
	$StarShooter.margin_left *= (OS.get_real_window_size().x / 1080)
	$Background.scale *= (OS.get_real_window_size() / Vector2(1080, 720))
	$Background.position *= (OS.get_real_window_size() / Vector2(1080, 720))
	$StarConnect/Credits.rect_scale *= (OS.get_real_window_size() / Vector2(1080, 720))
	$StarConnect/Credits.rect_position *= (OS.get_real_window_size() / Vector2(1080, 720))
	$StarConnect/Star.global_position *= (OS.get_real_window_size() / Vector2(1080, 720))
	$StarConnect/Star.set_physics_process(false)
	$StarShooter/Credits.rect_scale *= (OS.get_real_window_size() / Vector2(1080, 720))
	$StarShooter/Credits.rect_position *= (OS.get_real_window_size() / Vector2(1080, 720))
	$StarShooter/Player.global_position *= (OS.get_real_window_size().y / 720)
	$StarShooter/Player.set_physics_process(false)
	$StarConnect/Main/Play.grab_focus()
	
	for b in get_tree().get_nodes_in_group("buttons"):
		b.connect("focus_entered", self, "_menu_move")

func _menu_move() -> void:
	print("PLAYING MENU MOVE")
	AUDIO.play_se("MOVE_MENU", 12)

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_right") and current == 0 and $StarConnect.current == null:
		go_right()
	elif event.is_action_pressed("ui_left") and current == 1 and $StarShooter.current == null:
		go_left()

func go_left() -> void:
	$Animation.play_backwards("go_right")
	current -= 1

func go_right() -> void:
	$Animation.play("go_right")
	current += 1

func _on_Left_pressed():
	go_left()


func _on_Right_pressed():
	go_right()
