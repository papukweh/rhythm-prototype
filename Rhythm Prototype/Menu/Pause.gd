extends Control

func _ready():
	for b in get_tree().get_nodes_in_group("buttons"):
		b.connect("focus_entered", self, "_menu_move")

func _menu_move() -> void:
	print("PLAYING MENU MOVE")
	AUDIO.play_se("MOVE_MENU", 12)

func enter() -> void:
	get_tree().paused = true
	show()
	$Panel/VBoxContainer/Resume.grab_focus()

func exit() -> void:
	get_tree().paused = false
	hide()

func _on_Restart_pressed() -> void:
	exit()
	AUDIO.play_se("ENTER_MENU", 12)
	get_tree().change_scene("res://Main/Main.tscn")


func _on_Quit_pressed() -> void:
	exit()
	AUDIO.play_se("ENTER_MENU", 12)
	get_tree().change_scene("res://Menu/Main_Menu.tscn")

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		if get_tree().paused:
			exit()
		else:
			enter()

func _on_Resume_pressed():
	AUDIO.play_se("ENTER_MENU", 12)
	exit()


func _on_SFX_Setting_value_changed(value):
	AUDIO.set_sfx_volume(value)


func _on_Music_Setting_value_changed(value):
	AUDIO.set_bgm_volume(value)
	get_parent().get_parent().get_node("SongPlayer").volume_db = value
