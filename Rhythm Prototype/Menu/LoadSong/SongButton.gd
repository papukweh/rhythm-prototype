extends Button

var format_string : String = "{artist} - {song}"
var id : int

signal song_button_pressed

func set_info(id_arg: int, artist: String, song: String) -> void:
	id = id_arg
	var text = format_string.format({"artist":artist, "song":song})
	self.set_text(text)


func _on_Button_toggled(button_pressed: bool):
	print('toggle')
	emit_signal("song_button_pressed", id, button_pressed, self)
