extends Control

var current_song_id = 0
var current_dificulty = 0
var current_button = null
var first = true
var songs = []
const SONGS_PATH = "./Songs/"
const METADATA_PATH = "./Songs/Metadata/"

var SONG_BUTTON_SCENE = preload("res://Menu/LoadSong/SongButton.tscn")
signal finished_loading
signal finished_processing

func _ready():
	var dificulty = $Overview/Info/CenterContainer/Difficulty
	dificulty.add_item('Easy')
	dificulty.add_item('Normal')
	dificulty.add_item('Hard')
	dificulty.add_item('Very Hard')
	first = true

# Called when the node enters the scene tree for the first time.
func load_songs(userdata):
	if not first:
		emit_signal("finished_loading")
		return
	var song_file_list = GLOBAL.list_files_in_directory(SONGS_PATH, '.mp3')
	var song_menu = $Overview/ScrollContainer/Songs
	var i = 0
	for song_file in song_file_list:
		var song_path = SONGS_PATH+song_file
		var metadata_path = METADATA_PATH+song_file.replace('.mp3', '.json')
		var metadata = GLOBAL.load_metadata_info(metadata_path)
		if not metadata:
			print("No metadata, will create")
			var res = $Beatmapper.read_song_metadata(song_path)
			metadata = {
				'title': res[0],
				'artist': res[1],
				'bpm': res[2],
				'length': res[3],
				'onsets': {
					'Connect': [[], res[4], [], []],
					'Shooter': [res[4], [], [], []]
				},
				'high_score': [0, 0, 0]
			}
			GLOBAL.save_song_metadata(metadata_path, metadata)
		songs.append(Song.new(metadata['title'], metadata['artist'], 
			song_path, metadata['bpm'], metadata['length'],
			metadata['onsets'], metadata_path, metadata['high_score']))
		var song_button = SONG_BUTTON_SCENE.instance()
		song_menu.add_child(song_button)
		song_button.set_info(i, metadata['artist'], metadata['title'])
		song_button.connect("song_button_pressed", self, "on_Song_Selected")
		song_button.connect("focus_entered", self, "_menu_move")
		i += 1
	$Overview/ScrollContainer/Empty.hide()
	AUDIO.stop_bgm()
	$Overview/ScrollContainer/Songs.get_child(0).grab_focus()
	$Overview/ScrollContainer/Songs.get_child(0).set_pressed(true)
	first = false
	emit_signal("finished_loading")

func enter(game_mode: int) -> void:
	GLOBAL.GAME_MODE = game_mode
	show()
	$Animations.play("loading_songs")
	$Overview/ScrollContainer/Empty.show()
	var thread = Thread.new()
	thread.start(self, "load_songs", null)
	yield(self, "finished_loading")
	thread.wait_to_finish()

func exit() -> void:
	$SongPlayer.stop()
	AUDIO.play_bgm("MENU_THEME", true)
	if current_button:
		current_button.set_pressed(false)
	hide()

func on_Song_Selected(id: int, selected: bool, button: Button) -> void:
	AUDIO.play_se("ENTER_MENU", 12)
	print('on_song_selected '+str(id)+', '+str(selected))
	if selected:
		if current_button:
			current_button.set_pressed(false)
		current_button = button
		current_song_id = id
		current_button.set_pressed(true)
		$SongPlayer.play_song_preview(songs[id])
		set_metadata(songs[id])
	else:
		$SongPlayer.stop()
		current_song_id = 0
		current_button = null
		set_metadata(null)

func set_metadata(song: Song) -> void:
	print("set_metadata")
	var metadata = $Overview/Info/Metadata
	var artist_str = 'Artist: {name}'
	var song_str = 'Title: {name}'
	var duration_str = 'Duration: {num} segs'
	var hi_score = 'High Score: {score}'
	var has_image = false
	if song:
		print("has song")
		metadata.get_node("Artist").set_text(artist_str.format({'name':song.artist}))
		metadata.get_node("Song").set_text(song_str.format({'name':song.title}))
		metadata.get_node("Length").set_text(duration_str.format({'num': str(int(song.duration))}))
		metadata.get_node("BPM").set_text(str(int(song.BPM))+' BPM')
		metadata.get_node("High_Score").set_text(hi_score.format({'score': str(song.high_score[GLOBAL.GAME_MODE])}))
		if song.high_score[0] > 0:
			has_image = true
	else:
		print("no song")
		metadata.get_node("Artist").set_text(artist_str.format({'name':''}))
		metadata.get_node("Song").set_text(song_str.format({'name':''}))
		metadata.get_node("Length").set_text(duration_str.format({'num': '0'}))
		metadata.get_node("BPM").set_text('0 BPM')
		metadata.get_node("High_Score").set_text(hi_score.format({'score': '0'}))
	if has_image:
		var screenpath = song.metadataPath.replace('.json', '.png')
		var tex = ImageTexture.new()
		tex.load(screenpath)
		metadata.get_node("Image").set_texture(tex)
	else:
		var tex = load("res://Assets/background_small.jpeg")
		metadata.get_node("Image").set_texture(tex)

func _menu_move() -> void:
	print("PLAYING MENU MOVE")
	AUDIO.play_se("MOVE_MENU", 12)

func calculate_onsets(userdata):
	GLOBAL.initial_map = $Beatmapper.calculate_onsets(GLOBAL.SONG.audioPath.replace('.mp3', '.ogg'), GLOBAL.GAME_MODE, GLOBAL.DIFICULTY)
	emit_signal("finished_processing")

func _on_Play_pressed():
	print("PLAY!!")
	print(current_song_id)
	print(songs[current_song_id].title)
	GLOBAL.SONG = songs[current_song_id]
	GLOBAL.DIFICULTY = $Overview/Info/CenterContainer/Difficulty.get_selected_id()
	print("DIFICULDADE ", GLOBAL.DIFICULTY)
	if not GLOBAL.SONG.get_beatmap(GLOBAL.MODE_NAMES[GLOBAL.GAME_MODE], GLOBAL.DIFICULTY):
		print("OOPS, NO BEATMAP, WILL CREATE")
		$Animations.play("loading_beats")
		$Empty.show()
		for b in $Overview/ScrollContainer/Songs.get_children():
			b.disabled = true
		$Overview/Info/CenterContainer/Play.disabled = true
		var thread = Thread.new()
		thread.start(self, "calculate_onsets", null)
		yield(self, "finished_processing")
		thread.wait_to_finish()
		GLOBAL.SONG.set_beatmap(GLOBAL.MODE_NAMES[GLOBAL.GAME_MODE], GLOBAL.DIFICULTY, GLOBAL.initial_map)
		GLOBAL.update_metadata(GLOBAL.SONG)
	AUDIO.stop_bgm()
	get_tree().change_scene("res://Main/Main.tscn")
