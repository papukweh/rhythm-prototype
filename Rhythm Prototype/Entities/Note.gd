extends Node

class_name Note

const PLAYER_INPUT_CLASS = preload("res://Entities/PlayerInput.gd")
const RADIUS = 80
const MOMENTUM = 0.75

# All time units in ms
var spawnTime : float
var pressTime : float
var releaseTime: float
var input: PlayerInput
var position: Vector2
var direction: int

const DIRECTIONS = ['UP', 'UP-RIGHT', 'RIGHT', 'DOWN-RIGHT', 
		'DOWN', 'DOWN-LEFT', 'LEFT', 'UP-LEFT']
const MOVE_ATOM = 100

# Called when the node enters the scene tree for the first time.
func _init(spawnTimeArg: float, pressTimeArg: float, prevPos: Vector2, 
	prevDir: int, prevOnset: float):
	self.input = PLAYER_INPUT_CLASS.new()
	self.spawnTime = spawnTimeArg
	self.pressTime = pressTimeArg
	self.releaseTime = pressTime + 1
	
	var oldPos = prevPos
	var oldDir = prevDir
	
	var boost = 1
	boost = max(1, min(10*(spawnTime - prevOnset), 3))
	var MOVE_UNIT = MOVE_ATOM * boost
	print("MOVE_UNIT")
	print(MOVE_UNIT)
	var directionMethod = {
		'UP': Vector2(oldPos.x, oldPos.y - MOVE_UNIT),
		'UP-RIGHT': Vector2(oldPos.x + MOVE_UNIT / sqrt(2), oldPos.y - MOVE_UNIT / sqrt(2)),
		'RIGHT': Vector2(oldPos.x + MOVE_UNIT, oldPos.y),
		'DOWN-RIGHT': Vector2(oldPos.x + MOVE_UNIT / sqrt(2), oldPos.y + MOVE_UNIT / sqrt(2)),
		'DOWN': Vector2(oldPos.x, oldPos.y + MOVE_UNIT),
		'DOWN-LEFT': Vector2(oldPos.x - MOVE_UNIT / sqrt(2), oldPos.y + MOVE_UNIT / sqrt(2)),
		'LEFT': Vector2(oldPos.x - MOVE_UNIT, oldPos.y),
		'UP-LEFT': Vector2(oldPos.x - MOVE_UNIT / sqrt(2), oldPos.y + MOVE_UNIT / sqrt(2))
	}
	randomize()
	var keepMomentum = randf() <= 0.7
	var chosenDir = prevDir
	if keepMomentum:
		chosenDir = prevDir + int(rand_range(-1, 1))
		if chosenDir == -1:
			chosenDir = len(DIRECTIONS) - 1
	else:
		chosenDir = randi() % len(DIRECTIONS)
	var chosenPosition = directionMethod[DIRECTIONS[chosenDir]]
	self.position = chosenPosition
	self.direction = chosenDir
