extends Node

# Streams on which we'll play tue audio
var music = null
var audio = []
var prev_pos = 0.0

# Loads all our songs and SFXs
var MENU_THEME = load("res://Assets/BGM/Space Music.wav")
var HIT = load("res://Assets/SFX/Single-clap-sound-effect.ogg")
var MISS = load("res://Assets/SFX/253174__suntemple__retro-you-lose-sfx.wav")
var ENTER_MENU = load("res://Assets/SFX/Menu Confirm.ogg")
var MOVE_MENU = load("res://Assets/SFX/Menu Move.ogg")
var SHOOT = load("res://Assets/SFX/Shooting_Star.ogg")
var DODGE = load("res://Assets/SFX/420998__eponn__boost.wav")
var VICTORY = load("res://Assets/SFX/336725__kubatko__inception-horn-victory.wav")
var DEFEAT = load("res://Assets/SFX/518852__mrickey13__death-tune.ogg")
var CRACK = load("res://Assets/SFX/446116__justinvoke__crack-3.wav")
var EXPLOSION = load("res://Assets/SFX/33245__ljudman__grenade.wav")

# Variables used to play sounds
var songs = {'MENU_THEME':MENU_THEME}
var sounds = {
	'HIT': HIT,
	'MISS': MISS,
	'DODGE': DODGE,
	'ENTER_MENU': ENTER_MENU,
	'MOVE_MENU': MOVE_MENU, 
	'SHOOT': SHOOT,
	'CRACK': CRACK,
	'EXPLOSION': EXPLOSION,
	'DEFEAT': DEFEAT,
	'VICTORY': VICTORY
}

# Base volumes
onready var base_master = -10
onready var base_bgm = 5
onready var base_se = 8

func _ready():
	initSound()

# Initializes or resets the sounds system
func initSound():
	if audio.size() == 0:
		for i in range(10):
			audio.push_back(AudioStreamPlayer.new())
			self.add_child(audio[i])
			audio[i].volume_db = base_master
			audio[i].pause_mode = PAUSE_MODE_PROCESS
	else:
		audio[0].stop()
	if music == null:
		music = AudioStreamPlayer.new()
		self.add_child(music)
		music.stream = MENU_THEME
		music.volume_db = base_master + base_bgm
		music.play()
		music.pause_mode = PAUSE_MODE_PROCESS
	elif music.stream != MENU_THEME:
		music.stream = MENU_THEME
		music.volume_db = base_master + base_bgm
		music.play()
	else:
		return


# Resets the volume on all audio streams
func recalibrate():
	music.volume_db = base_master + base_bgm
	for i in range(10):
		audio[i].volume_db = base_master + base_se


# Stops playing a BGM
func stop_bgm():
	prev_pos = music.get_playback_position()
	print(prev_pos)
	music.stop()


# Plays a bgm. If a keep argument is sent, it will continue playing
# the song from current position
func play_bgm(bgm, keep=false, loud=0):
	if music.stream == songs[bgm] and not keep:
		return
	var play = 0.0
	music.stream = songs[bgm]
	music.volume_db = base_master + base_bgm + loud
	if keep:
		play = prev_pos
	music.play(play)


# Plays a sound effect. The loud argument is an extra to raise volume.
func play_se(sound_arg, loud=-16):
	var sound = sounds[sound_arg]
	for i in range(10):
		if !audio[i].playing:
			audio[i].volume_db = base_master + base_se + loud
			audio[i].stream = sound
			if sound_arg == "HIT":
				audio[i].pitch_scale = rand_range(0.8, 1.2)
			else:
				audio[i].pitch_scale = 1.0
			audio[i].play()
			return

############# CONFIG FUNCTIONS ###################
func get_master_volume():
	return base_master

func get_bgm_volume():
	return base_bgm

func get_sfx_volume():
	return base_se

func set_master_volume(vol):
	base_master = vol
	recalibrate()

func set_sfx_volume(vol):
	base_se = vol
	recalibrate()

func set_bgm_volume(vol):
	base_bgm = vol
	recalibrate()
