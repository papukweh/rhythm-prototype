extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	$Panel/RedStar.color = Color.red
	$Panel/BlueStar.color = Color.blue
	$Panel/RedStar.set_physics_process(false)
	$Panel/BlueStar.set_physics_process(false)

func enter():
	show()
	$Animation.play("tutorial")

func exit():
	hide()
	$Animation.stop()
