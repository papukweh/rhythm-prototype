extends Camera2D

const PADDING = 100
var game_mode : int = 0

func init(game_mode: int) -> void:
	if game_mode == 1:
		zoom = Vector2(2.5, 2.5)
		set_process(false)
	else:
		zoom = Vector2(1.25, 1.25)

func prepare_for_screenshot(line: Line2D) -> void:
	center_on(line.points, true)
	print("SCRENSHOT CENTER")
	print(self.global_position)
	set_process(false)

func _process(delta):
	var notes = get_tree().get_nodes_in_group("notes")
	if notes.size() > 0:
		center_on(notes, false)


func center_on(things: Array, should_zoom: bool) -> void:
	var sum_pos = Vector2(0,0)
	var max_pos = Vector2(-INF, -INF)
	var min_pos = Vector2(INF, INF)

	#"Cheap" way to get the player center is
	#to calculate the median

	#Sum all players positions
	for thing in things:
		var player_pos = thing
		if typeof(thing) != TYPE_VECTOR2:
			player_pos = thing.get_global_position()
		sum_pos += player_pos

		if player_pos.x > max_pos.x:
			max_pos.x = player_pos.x

		if player_pos.x < min_pos.x:
			min_pos.x = player_pos.x

		if player_pos.y > max_pos.y:
			max_pos.y = player_pos.y

		if player_pos.y < min_pos.y:
			min_pos.y = player_pos.y

	#Value used to correct padding
	var correct_pixel = (get_viewport().size/100) * PADDING

	#Calculate Position
	sum_pos /= float(things.size())
	
	#print("SUMDIVIDE")
	#print(sum_pos)
	
	#print("MINMAX")
	var new_pos = (max_pos + min_pos) / 2
	#print(new_pos)
	
	self.global_position = sum_pos
	#print("NEW POSITION")
	#print(sum_pos)
	
	if should_zoom:
		#Calculate Zoom
		var diff_pos = max_pos - min_pos
		var diff_zoom = Vector2((diff_pos.x)/get_viewport().size.x,
								(diff_pos.y)/get_viewport().size.y)
	
		var new_zoom = max( max(diff_zoom.x, diff_zoom.y), 1 )
		self.zoom = Vector2(new_zoom,new_zoom)
