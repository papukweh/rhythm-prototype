extends Node2D

const initialScale = 1.0
const idealScale = 0.5

var idealTime : float
var elapsedTime : float

var currentScale
var input
var over = false
var score = 0.0
var current = false
export var color : Color = Color.red

signal NOTE_HIT

func _ready():
	$Star.color = color

func init(spawnTime: float, pressTime: float, input_arg: String, position_arg: Vector2) -> void:
	idealTime = pressTime - spawnTime
	elapsedTime = 0.0
	self.global_position = position_arg
	input = input_arg
	currentScale = initialScale
	color = PlayerInput.get_color(input)
	$Star.color = color

func die_and_calculate_score(miss: bool = false):
	if miss:
		emit_signal("NOTE_HIT", input, "MISS")
		AUDIO.play_se("MISS", -2)
	else:
		score = currentScale/idealScale
		currentScale = 0.0
		over = true
		print("SCORE: "+str(score))
		var grade = get_grade(score)
		emit_signal("NOTE_HIT", input, grade)
		if grade == "MISS":
			AUDIO.play_se("MISS", -2)
		else:
			AUDIO.play_se("HIT", -18)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	update_and_check_radius(delta*1000)
	update()

func update_and_check_radius(deltaMs: float):
	elapsedTime += deltaMs
	currentScale = initialScale - ((elapsedTime/idealTime) * (initialScale - idealScale))
	$StarOutline.scale = Vector2(currentScale, currentScale) 

	if currentScale - idealScale <= -initialScale*0.25:
		die_and_calculate_score()


func get_grade(score_arg):
	var diffAbs = abs(1.0 - score_arg)
	if diffAbs <= 0.1:
		return "EXCELLENT"
	elif diffAbs <= 0.15:
		return "GREAT"
	elif diffAbs <= 0.25:
		return "GOOD"
	elif diffAbs <= 0.5:
		return "BAD"
	else:
		return "MISS"
