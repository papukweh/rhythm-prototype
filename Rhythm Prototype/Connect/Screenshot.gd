extends Node

signal screenshot_taken

func take_screenshot(save_path: String):
	var old_clear_mode = get_viewport().get_clear_mode()
	get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
	# Let two frames pass to make sure the screen was captured.
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	
	# Retrieve the captured image.
	var img = get_viewport().get_texture().get_data()
	# restore the previous value, as some part wont redraw after...
	get_viewport().set_clear_mode(old_clear_mode)
	
	# Flip it on the y-axis (because it's flipped).
	img.flip_y()
	
	# I also want a thumbnail 1/3 size
	var s = img.get_size()
	var scale_reduce = 0.3
	img.resize(600, 375)
	img.save_png(save_path)
	print("SCREENSHOT TAKEN!")
	emit_signal("screenshot_taken")
