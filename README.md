# Rhythm Prototype

Prototype rhythm games developed for my undergrad project
This project uses the [godot-python addon](https://github.com/touilleMan/godot-python) in order to process user songs with the [librosa library](https://librosa.org/).

The process of choosing algorithms to create the beatmap can be found on this [other repository](https://gitlab.com/papukweh/mir-experiments).

## Adding Custom Songs
Simply drop your .mp3 files (only this extension is supported currently) on the `Songs` folder.

## Games

The goal of this project is to develop simple rhythm game prototypes using custom songs in the span of a month.

All feedback is appreciated :)

### 1. Star Connect (v2)
Press the buttons on the correct time in order to create a beautiful constelation in the night sky.

After receiving feedbacks, the second version has now released!

Changelog:
```
- Added different SFX for hit and miss
- Added "How to play" button on the Main Menu
- Added Volume sliders
- Fixed wrong star being pressed
- Fixed song with title too long on the menu bug
- Added new difficulty options (Easy, Normal, Hard and Very Hard)
- Added lazy loading
- Added new preloaded songs
- Included ffmpeg binary on build
```

### 2. Star Shooter (v1)
Dodge all the shooting stars falling from the skies in order to survive and aim for "Close calls" to improve your score.

First version has now released!

